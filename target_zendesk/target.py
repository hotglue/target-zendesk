"""Zendesk target class."""

from singer_sdk import typing as th
from target_hotglue.target import TargetHotglue

from target_zendesk.sinks import (
    TicketsSink,
)


class TargetZendesk(TargetHotglue):
    """Sample target for Zendesk."""

    SINK_TYPES = [
        TicketsSink
    ]

    name = "target-zendesk"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "subdomain",
            th.StringType,
            required = True
        ),
        th.Property(
            "access_token",
            th.StringType,
            required = True
        ),
    ).to_dict()

if __name__ == '__main__':
    TargetZendesk.cli()
