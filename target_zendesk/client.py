from target_hotglue.client import HotglueSink

class ZendeskSink(HotglueSink):


    @property
    def base_url(self) -> str:
        if self.config.get("subdomain"):
            return f"https://{self.config.get('subdomain')}.zendesk.com/api/v2/"
        else:
            raise Exception("No subdomain provided")
    

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers['Authorization'] = f"Bearer {self.config.get('access_token')}"
        return headers