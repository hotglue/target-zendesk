"""Zendesk target sink class, which handles writing streams."""

import singer

LOGGER = singer.get_logger()
from target_zendesk.client import ZendeskSink


class TicketsSink(ZendeskSink):
    """Zendesk target sink class."""

    name = "Tickets"

    def preprocess_record(self, record: dict, context: dict) -> dict:
        payload = {
            "subject": record.get("title"),
            "priority": record.get("priority", "normal").lower(),
            "due_at": record.get("due_date"),
            "type": record.get("task_type", "task").lower(),
            "assignee_email": record.get("assignee_email"),
            "tags": record.get("tags"),
        }

        if record.get("description"):
            payload["comment"] = {"body": record["description"]}

        # Allowed values are "new", "open", "pending", "hold", "solved", or "closed".
        if record.get("status"):
            payload["status"] = record.get("status").lower()

        if record.get("reporter_email"):
            payload["requester"] = {
                "name": record.get("reporter_name"),
                "email": record["reporter_email"],
            }

        if record.get("id"):
            payload["id"] = record["id"]

        return payload

    def upsert_record(self, record: dict, context: dict):

        endpoint = "tickets"
        method = "POST"
        state_dict = dict()
        id = None
        if "id" in record:
            id = record["id"]
            endpoint = f"{endpoint}/{record['id']}"
            del record["id"]
            method = "PUT"
            if "type" in record:
                del record["type"]

        response = self.request_api(
            http_method=method, request_data={"ticket": record}, endpoint=endpoint
        )
        if response.status_code in [200, 201]:
            state_dict["success"] = True
            id = response.json()["ticket"].get("id")
        elif response.status_code == 204 and method == "PUT":
            state_dict["is_updated"] = True
        return id, response.ok, state_dict
